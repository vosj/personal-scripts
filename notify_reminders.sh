#!/usr/bin/env dash

# name_of_script.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


export DISPLAY=:0

current_year=$(date +%Y)
current_day=$(date +%j)

reminder_directory="/home/jeff/doc/"
reminder_file="reminders-$current_year.txt"
reminder_full_path="$reminder_directory$reminder_file"

from_address="reminders@example.com"
to_address="jeff@example.com"
tmp_mail_file="/tmp/notify_reminder.txt"
remote_mail_login="root@mail.example.com"

alert_file="/home/jeff/sfx/fanfare2.wav"

build_notification() {
	reminder_subject=$(echo $reminder_string | awk -F ":" '{print $3}')
	reminder_message=$(echo $reminder_string | awk -F ":" '{print $4}')
}

system_notification() {
	notify-send -u critical "$reminder_subject" "$reminder_message"
	notification_sent=true
}

mail_notification() {
	ssh $remote_mail_login "echo \"$reminder_message\" > $tmp_mail_file"
	ssh $remote_mail_login "mail -s \"$reminder_subject\" -r \"$from_address\" \"$to_address\" < $tmp_mail_file"
}

counter=1
while :; do
	reminder_string=$(cat $reminder_full_path | grep "$current_day:$counter")
	if [ -n "$reminder_string" ]; then
		build_notification
		system_notification
		mail_notification
	else
		break
	fi
	counter=$(($counter + 1))
done

if [ -n "$notification_sent" ]; then
	current_volume=$(sndioctl output.level)
	sndioctl output.level=0.6
	mplayer $alert_file
	sndioctl $current_volume
fi
