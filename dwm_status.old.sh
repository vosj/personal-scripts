#!/usr/bin/env sh

# dwm_status.sh
# Copyright (c) 2021 by Jeff Vos <jeff@jeffvos.dev>




#
# PREFERENCE VARIABLES
#

# number of seconds each iteration lasts
iteration_length=1

# OS INFORMATION SETTINGS
# set to any value other than 1 to disable OS info display
display_os=1
# number of iterations to display OS information each pass
os_iterations=3

# TIME DISPLAY SETTINGS
# set to any value other than 1 to disable time display
display_time=1
# number of iterations to display time information each pass
time_iterations=5

# DATE DISPLAY SETTINGS
# set to any value other than 1 to disable date display
display_date=1
# number of iterations to display date information each pass
date_iterations=5

# TRANSITION SETTINGS
# set to any value other than 1 to disable transitions
display_transitions=1
# set to any value other than 1 to run half cycles instead of full
transition_style=1
# display duration of each stage of the transition
transition_speed=0.05
# number of times longer the beginning and ending buffers are
# than the actual transition frames
buffer_time=3
# moving character to display
tch1="/"
# potentially different character to display
# for the second half of the animation
tch2='\'
# opening bracket character
topen="["
# closing bracket character
tclose="]"
# filler character
t="-"




# store OS info in $os_info variable
os_info=$(uname -sr)

transition_display() {
	if [ "$display_transitions" -eq "1" ]
	then
		tch=$tch1
		xsetroot -name " "
		sleep $(echo "$transition_speed*$buffer_time" | bc)
		xsetroot -name " $topen$t$t$t$t$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$tch$t$t$t$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$tch$t$t$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$tch$t$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$tch$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$tch$t$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$tch$t$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$t$tch$t$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$t$t$tch$t$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$t$t$t$tch$t$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$t$t$t$t$tch$tclose "
		sleep $transition_speed
		xsetroot -name " $topen$t$t$t$t$t$t$t$t$t$t$tclose "
		sleep $transition_speed
		if [ "$transition_style" -eq "1" ]
		then
			tch=$tch2
			xsetroot -name " $topen$t$t$t$t$t$t$t$t$t$tch$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$t$t$t$t$tch$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$t$t$t$tch$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$t$t$tch$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$t$tch$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$tch$t$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$tch$t$t$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$tch$t$t$t$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$tch$t$t$t$t$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " $topen$t$t$t$t$t$t$t$t$t$t$tclose "
			sleep $transition_speed
			xsetroot -name " "
		fi
		sleep $(echo "$transition_speed*$buffer_time" | bc)
	fi
}

display_os_info() {

	# set the X root window name to the value of $os_info
	# dwm uses this name for its status area
	xsetroot -name " $os_info "
	os_iterations=$(expr $os_iterations \* $iteration_length)
	sleep $os_iterations
}


update_time() {

	# pull current time with date command in the "HH:MM AM/PM" format
	# and store in the variable $current_time
	current_time=$(date +" %I:%M %p ")

	# set the X root window name to the value of $current_time
	# dwm uses this name for its status area
	xsetroot -name "$current_time"

	# sleep for the number of seconds set in $iteration_length
	# before beginning next update
	sleep $iteration_length
}


update_date() {

	# pull current date with date command in the "Day MM/DD/YYYY" format
	# and store in the variable $current_date
	current_date=$(date +" %a %m/%d/%y ")

	# set the X root window name to the value of $current_date
	# dwm uses this name for its status area
	xsetroot -name "$current_date"

	# sleep for the number of seconds set in $iteration_length
	# before beginning next update
	sleep $iteration_length
}


reset_counter() {

	# reset $counter variable to 0
	counter=0
}


increment_counter() {

	# increment $counter variable by 1
	counter=$(expr $counter + 1)
}




# infinite loop to keep the updates going
while true
do
	if [ "$display_os" -eq "1" ]
	then
		transition_display
		display_os_info
	fi

	if [ "$display_time" -eq "1" ]
	then
		transition_display
		reset_counter

		# run update_time function the number of times defined
		# in the $time_iterations variable
		while [ "$counter" -lt "$time_iterations" ]
		do
			update_time
			increment_counter
		done
	fi

	if [ "$display_date" -eq "1" ]
	then
		transition_display
		reset_counter

		# run update_date function the number of times defined
		# in the $date_iterations variable
		while [ "$counter" -lt "$date_iterations" ]
		do
			update_date
			increment_counter
		done
	fi
done
