#!/usr/bin/env sh

# monitor_pause.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


display_pause_seconds=5
active_monitors=$(xrandr --listactivemonitors)

case $active_monitors in
	*DVI-0*)
		xrandr --output DVI-0 --off
		sleep $display_pause_seconds
		xrandr --output DVI-0 --auto
	;;
esac
