#!/usr/bin/env sh

# work_notifications.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>

export DISPLAY=:0

year=$(date +%Y)
day=$(date +%j)

schedule_directory="/home/jeff/.work/"
alarm_file="/home/jeff/sfx/chime_big_ben.wav"

work_check=$(cat ${schedule_directory}${year} | grep $day)

if [ "$work_check" == "$day" ]
then
	counter=6

	while [ "$counter" -ge 0 ]
	do
		time_left=$(expr $counter \* 10)

		if [ "$counter" -ge 2 ]
		then
			notify-send "Work Soon" "Get ready for work in $time_left minutes!"
		elif [ "$counter" -eq 1 ]
		then
			notify-send -u critical "Work Very Soon" "Get ready for work in $time_left minutes!"
		else
			notify-send -u critical "Get Ready Now" "It's time to get ready for work!"
			current_volume=$(sndioctl output.level)
			sndioctl output.level=0.6
			mplayer ${alarm_file}
			sndioctl ${current_volume}
		fi

		sleep 600

		counter=$(expr $counter - 1)
	done
fi
