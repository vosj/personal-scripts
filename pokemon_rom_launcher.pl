#!/usr/bin/env perl

# pokemon_rom_launcher.pl
# launcher script for gb/gbc/gba pokemon roms using dmenu and vbam
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>

use strict;
use warnings;
use utf8;
use v5.10;
use String::ShellQuote qw(shell_quote);

sub main() {
	# directory roms are stored in
	my $rom_directory = '/home/jeff/games/roms/';

	# store available roms in the array
	my @roms = ( "red", "blue", "yellow", "silver", "gold", "crystal", "ruby", "sapphire", "emerald", "fire_red", "leaf_green", );

	# filter to use in vbam
	my $vbam_filter = '13';

	# define roms that use the *.gb extension
	my @gb = ( "red", "blue", );
	# define roms that use the *.gba extension
	my @gbc = ( "yellow", "silver", "gold", "crystal", );

	# build dmenu string from rom array
	my $dmenu_list = "";
	foreach my $rom (@roms) {
		$dmenu_list = $dmenu_list . $rom . "\n";
	}
	chomp $dmenu_list;

	# select rom from array using dmenu
	my $dmenu_cmd = "echo " . shell_quote($dmenu_list) . " | dmenu -h 50";
	my $result = `$dmenu_cmd`;
	chomp $result;

	# select correct rom extension based off selection
	# if rom name is not in the @gb or @gbc arrays,
	# a *.gba extension will be assumed
	my $extension;
	if(grep(/^$result$/, @gb)) {
		$extension = '.gb';
	} elsif(grep(/^$result$/, @gbc)) {
		$extension = '.gbc';
	} else {
		$extension = '.gba';
	}

	# launch rom.
	system 'vbam', '-f', $vbam_filter, $rom_directory . 'pokemon_' . $result . $extension;
}

main();

