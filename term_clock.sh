#!/usr/bin/env sh

# term_clock.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


my_uname=$(uname -sr)

while :
do
	my_date=$(date +"%a %m/%d/%y")
	my_time=$(date +"%I:%M:%S %p")
	my_uptime=$(uptime)
	my_check=$(date +%S)
	clear
	echo "$my_uname\n\n"
	echo "\t$my_date" | figlet
	echo "\t$my_time" | figlet
	echo "\n$my_uptime\n"
	while :
	do
		sleep 0.01
		if [ "$my_check" != "$(date +%S)" ]
		then
			break
		fi
	done
done
