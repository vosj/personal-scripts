#!/usr/bin/env sh

# dwm_status.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


# PREFERENCES

# OS DISPLAY
# Display OS information; set to true or false
display_os=true
# Arguments to pass to uname command
uname_args="-sr"
# OS display length; number of seconds to display
os_length=3

# TIME DISPLAY
# Display time information; set to true or false
display_time=true
# Time display length; number of seconds to display
time_length=5

# DATE DISPLAY
# Display date information; set to true or false
display_date=true
# Date display length; number of seconds to display
date_length=5

# TRANSITION DISPLAY
# Display animated transitions; set to true or false
display_transitions=true
# Transition cycle length; set to full or half
transition_duration=full
# Transition speed; measured in seconds each frame is displayed
# Lower values result in a faster animation
transition_speed=0.05
# Transition buffer multiplier; number of times to display beginning
# and ending frames compared to animation frames
transition_buffer=5
# Transition animation characters
transition_opening_bracket='['
transition_closing_bracket=']'
transition_filler_character='-'
transition_moving_character='+'

# Set to true to display output in terminal instead of setting root window name
test_in_terminal=false


transition_base="$transition_opening_bracket"
counter=0
while [ "$counter" -lt 10 ]; do
	transition_base="$transition_base$transition_filler_character"
	counter=$(expr $counter + 1)
done
transition_base="$transition_base$transition_closing_bracket"

display_frame() {
	if [ "$test_in_terminal" = "true" ]; then
		clear; echo $frame_string
	else
		xsetroot -name "$frame_string"
	fi
	sleep $frame_length
}

build_transition() {
	case $transition_frame in
		0 | 11 | 22)
			transition_string=$transition_base ;;
		1 | 21)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/2") ;;
		2 | 20)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/3") ;;
		3 | 19)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/4") ;;
		4 | 18)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/5") ;;
		5 | 17)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/6") ;;
		6 | 16)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/7") ;;
		7 | 15)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/8") ;;
		8 | 14)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/9") ;;
		9 | 13)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/10") ;;
		10 | 12)
			transition_string=$(echo $transition_base | sed "s/./$transition_moving_character/11") ;;
	esac

	frame_string=" $transition_string "
	frame_length=$transition_speed
	display_frame
}

transition_delay=$(echo "$transition_speed * $transition_buffer" | bc)

display_transition() {
	if [ "$display_transitions" = "true" ]; then
		case $transition_duration in
			half) transition_frames=11 ;;
			full) transition_frames=22 ;;
		esac

		transition_frame=0
		while [ "$transition_frame" -le "$transition_frames" ]; do
			build_transition
			case $transition_frame in
				0 | 22)
					sleep $transition_delay ;;
				11)
					if [ "$transition_duration" = "half" ]; then
						sleep $transition_delay
					fi ;;
			esac
			transition_frame=$(expr $transition_frame + 1)
		done
	fi
}


os_information=$(uname $uname_args)

display_os() {
	if [ "$display_os" = "true" ]; then
		frame_string=" $os_information "
		frame_length="$os_length"
		display_frame
	fi
}


display_time() {
	if [ "$display_time" = "true" ]; then
		time_information=$(date +"%I:%M %p")
		frame_string=" $time_information "
		frame_length="$time_length"
		display_frame
	fi
}


display_date() {
	if [ "$display_date" = "true" ]; then
		date_information=$(date +"%a %m/%d/%y")
		frame_string=" $date_information "
		frame_length="$date_length"
		display_frame
	fi
}

while :; do
	display_os
	display_transition
	display_time
	display_transition
	display_date
	display_transition
done
