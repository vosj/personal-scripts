#!/usr/bin/env bash

# toggle_display.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


export DISPLAY=:0

if [[ $(xrandr --listactivemonitors) == *DVI-0* ]]; then
	xrandr --output DVI-0 --off
else
	xrandr --output DVI-0 --auto
fi
