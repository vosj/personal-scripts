#!/usr/bin/env sh

# syspatch_check.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


syspatch_check=$(doas syspatch -c)
alert_file="/home/jeff/sfx/train_crossing.wav"

if [ "$syspatch_check" == "" ]
then
	return 1
else
	notify-send -u critical "syspatch" "System patch is available! Please run 'syspatch' as root to install."
	current_volume=$(sndioctl output.level)
	sndioctl output.level=0.6
	mplayer ${alert_file}
	sndioctl ${current_volume}
fi
