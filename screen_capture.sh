#!/usr/bin/env sh

# screen_capture.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


capture_directory="/home/jeff/pics/screenshots/"
capture_base="screenshot"
capture_date=$(date +"_%Y-%m-%d_%H%M%S")
capture_extension=".png"

maim "${capture_directory}${capture_base}${capture_date}${capture_extension}"
