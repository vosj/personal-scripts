#!/usr/bin/env sh

# post_to_clbin.sh
# Copyright (c) 2021 by Jeff Vos <jeff@jeffvos.dev>

if [ -e $1 ]
then
	curl -F "clbin=<$1" https://clbin.com
else
	echo "cannot send to clbin: file does not exist"
fi
