#!/usr/bin/env sh

# neofetch_refresh.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


while :
do
	clear
	neofetch
	sleep 300
done
