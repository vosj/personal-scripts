#!/usr/bin/env sh

# neomutt_passwords.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


gpg -qd ~/.config/neomutt/accounts/passwords.gpg | grep $1 | awk '{print $2}'
