#!/usr/bin/env sh

# workday_monitor_on.sh
# Copyright (c) 2021 Jeff Vos <jeff@jeffvos.dev>


export DISPLAY=:0

current_year=$(date +%Y)
current_day=$(date +%j)

schedule_directory="/home/jeff/.work/"

work_check=$(cat ${schedule_directory}${year} | grep $current_day)

if [ "$work_check" == "$current_day" ]
then
	xrandr --output DVI-0 --auto
fi
